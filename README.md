Code examples for the presentation "Accelerating HVDC Test With NI and The MathWorks", given at NI Week 2017 in Austin, TX, and at LabVIEW Developer Days 2017 in Nürnberg, Germany, by Julian Lange (Siemens AG) and Joerg Hampel (HAMPEL SOFTWARE ENGINEERING.

The excerpt: "High voltage, direct current (HVDC) is key for long-distance electric power transmission. Learn how engineers used the NI platform to implement a proof of concept for a revolutionary HVDC test circuit at record speed. Discover how to integrate a high-speed The MathWorks, Inc. MATLAB® and Simulink® model into LabVIEW Real-Time and LabVIEW FPGA."

Read more at:
- https://www.hampel-soft.com/blog/ni-week-2017/
- https://www.hampel-soft.com/blog/labview-dev-days-2017/